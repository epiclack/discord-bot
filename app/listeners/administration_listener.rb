module EpiclackBot
  module Listeners
    class AdministrationListener

      def initialize bot, config
        @bot = bot
        @config = config
      end

      def register
        @bot.command :admin do |event, *args|
          server = event.server
          user = event.user

          # Only Synapze
          if user.id == 311783424404881408
            key = "servers.#{server.id}.admin_group"

            @config.set key, args[0].to_i

            event << "Admin group set with success!"
          end
        end
      end
    end
  end
end
