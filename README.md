# Epiclack's Discord Bot

Discord Bot written in Ruby using ImageMagick

## Requirements

You need to have at least `bundler` installed. If you are unsure or you need to
install it, run:

```Shell
gem install bundler
```

This bot use ImageMagick, thus you also need to have it installed on your
system.

**On Arch Linux**

```Shell
pacman -Syy pkg-config imagemagick
```

### Install gems

The project use bundler to manage gems requirements. Simply run:

```Shell
bundle install
```

## Environment configuration

As every Discord Bot you will need to provide a token to authenticate with
Discord.

To create one, go to https://discord.com/developers/applications/.
Create a new application, this will be your application to develop the bot, a
good name may be: `epiclack-bot-dev`.

When this is done, click on your newly created application and go into the
`Bot` section. Follow the guidelines until you have a Token.

You have to different ways to give this token to your application:

* `EPICLACK_DISCORD_TOKEN`: environment variable with your token stored inside.
* `EPICLACK_DISCORD_TOKEN_FILE`: environment variable with the path to a file
  containing the token.

**Note**: We prioritized `EPICLACK_DISCORD_TOKEN_FILE` in lookup order.

### Basic environment variable

* `EPICLACK_DISCORD_REPOST_CHANNEL`: name of the respot channel without #

---

When this is done you can pat yourself, your are good to go!

## Run the bot

To launch the bot simply run:

```Shell
bundle exec ruby app/app.rb
```

## Contributing

We are welcoming every Merge Request, if you have any question you can directly
ask me one Epiclack Discord or by mail bastien.germond@epita.fr
