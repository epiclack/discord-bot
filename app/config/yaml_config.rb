require 'yaml'
require 'fileutils'

module EpiclackBot
  module Config
    class YamlConfig
      def initialize path = 'config.yml'
        unless File.exists? path
          $logger.info "Creating empty config.yml"
          FileUtils.touch path
        end

        @file = File.open path, 'r+'

        @config = YAML.load @file
        @config ||= Hash.new
      end

      def set key, value
        # Trick to use pathname
        key.gsub! '.', '/'

        path = Pathname.new key

        current = @config
        # Assure that there is a Hash at the end of the path
        path.dirname.descend do |p|
          basename = p.basename.to_s
          next if basename == ''

          unless current[basename] and current.is_a?(Hash)
            current[basename] = Hash.new
          end

          current = current[basename]
        end

        current[path.basename.to_s] = value

        save
      end

      def get key
        # Trick to use pathname
        key.gsub! '.', '/'

        path = Pathname.new key

        current = @config
        path.dirname.descend do |p|
          basename = p.basename.to_s
          next if basename == ''

          unless current[basename] and current.is_a?(Hash)
            return nil # not found
          end

          current = current[basename]
        end

        return current[path.basename.to_s]
      end

      def get_server server, key
        key = "servers.#{server.id}.#{key}"
        get key
      end

      def set_server server, key, value
        key = "servers.#{server.id}.#{key}"
        set key, value
      end

      def is_admin_on_server? server, user
        admin_group = get_server server, "admin_group"

        if admin_group
          admin_role = server.roles.filter do |role|
            role.id == admin_group
          end

          admin_role = admin_role.first

          if admin_role
            user_in_group = admin_role.users.filter do |u|
              u == user
            end

            if user_in_group.size != 0
              return true # User is admin !
            else
              $logger.warn "#{user.username} is not in admin group"
            end
          else
            $logger.warn "Couldn't find admin role set (#{admin_group}) on #{server.name}"
          end
        else
          $logger.warn "There is no admin group set for this server: #{server.name}"
        end

        return false
      end

      private

      def save
        @file.rewind # Rewrite from top
        @file.write(YAML.dump @config)
      end
    end
  end
end
