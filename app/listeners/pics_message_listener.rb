require_relative "../repost"

module EpiclackBot
  module Listeners
    class PicsMessageListener

      def initialize bot
        @bot = bot
      end

      def register
        @bot.message() do |event|
          if event.channel.name == "pics"
            # $logger.debug "New message in #pics channel"

            server = event.server
            message = event.message
            username = message.author.username
            attachments = message.attachments

            handle_repost server, attachments, username
          end
        end
      end
    end
  end
end
