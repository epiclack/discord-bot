require 'discordrb'
require 'stringio'
require 'rmagick'
require 'rest-client'

require_relative 'config/yaml_config'
require_relative 'listeners/administration_listener'
require_relative 'listeners/frame_reaction_add_listener'
require_relative 'listeners/n1tsu_autoreact_listener'
require_relative 'listeners/pics_message_listener'
require_relative 'repost'
require_relative 'utils'

# logger
$logger = Logger.new STDOUT

# Configuration
token = load_configuration_variable "EPICLACK_DISCORD_TOKEN"

$REPOST_CHANNEL_NAME = load_configuration_variable "EPICLACK_DISCORD_REPOST_CHANNEL"
$TRIGGER_REPOST_EMOJI = "🖼️" # this is :frame_photo:
$CONFIG_PATH = load_configuration_variable "EPICLACK_DISCORD_CONFIG"

bot = Discordrb::Commands::CommandBot.new token: token, prefix: '+'

config = EpiclackBot::Config::YamlConfig.new $CONFIG_PATH

EpiclackBot::Listeners::FrameReactionAddListener.new(bot, config).register
EpiclackBot::Listeners::PicsMessageListener.new(bot).register
EpiclackBot::Listeners::AdministrationListener.new(bot, config).register
EpiclackBot::Listeners::N1tsuAutoreactListener.new(bot, config).register

bot.run
