# Helper file

require 'logger'

##
# Load configuration variable
#
# Will look firstly for an environment variable named: +key_FILE+
# If there is one it will be prioritized, and will use the content of the file.
# In case there was any error during the loading of the previous file, it will
# display a warning and continue with the next step
#
# If there is no environment variable named +key+, an error message is written
# and nil is returned.
#
# Use Logger $logger if one exist or else create a local Logger on STDOUT
def load_configuration_variable key
  # Check logger
  logger = $logger || Logger.new(STDOUT)

  if ENV["#{key}_FILE"]
    path = ENV["#{key}_FILE"]
    # Load from file
    if File.exists? path
      file = File.open path, 'r'
      value = file.read.chomp!
      return value
    else
      logger.warn "Couldn't load environment configuration '#{key}' from #{path}."
    end
  end

  if ENV[key]
    return ENV[key]
  end

  logger.error ""
  logger.error "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
  logger.error "@ Coudln't load environment configuration:                         @"
  logger.error "@   #{key.ljust 62} @"
  logger.error "@ You should remove the load or set it to at least something.      @"
  logger.error "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
  logger.error ""

  return nil
end
