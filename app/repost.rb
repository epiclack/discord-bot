require 'rmagick'
require 'rest-client'

##
# Process picture, add watermark and signature
#
# Return Magick::Image
def process_picture image, signature
  watermark = Magick::Image.read("epiclack.png").first
  scale_size = 0.1 * image.columns
  watermark.resize_to_fit! scale_size

  image = image.composite watermark, Magick::SouthEastGravity, Magick::OverCompositeOp

  draw = Magick::Draw.new

  draw.pointsize = 0.3 * scale_size
  draw.font_weight = Magick::BoldWeight
  draw.font_style = Magick::ItalicStyle
  draw.gravity = Magick::SouthEastGravity
  draw.fill = 'white'

  draw.annotate image, 0, 0, scale_size, 0.25 * scale_size, signature

  return image
end

##
# Handle the repost process in the repost channel
#
# server (Discordrb::Server)
# attachments (Array<Discordrb::Attachment>)
# username (Ruby::String)
# repost_channel (Discordrb::Channel)
def handle_repost server, attachments, username, repost_channel: nil

  return unless attachments

  unless repost_channel
    repost_channel_index = server.channels.index do |c|
      c.name == $REPOST_CHANNEL_NAME
    end

    unless repost_channel_index
      $logger.warn "handle_repost: there is no ##{$REPOST_CHANNEL_NAME} on server: #{server.name}"
      return
    end

    repost_channel = server.channels[repost_channel_index]
  end

  attachments.each do |attachment|
    if attachment.image?
      image = RestClient.get attachment.url
      image = Magick::Image.from_blob(image).first
      signature = "@#{username}"

      image = process_picture image, signature

      tmpfile = Tempfile.new 'image.png'
      tmpfile.write image.to_blob
      tmpfile.rewind

      begin
        repost_channel.send_file(tmpfile, filename: "image.png")
      rescue IOError
        $logger.warn "IOError: Failed to send file"
      end
    else
      $logger.info "Not an image, skipping"
    end
  end
end
