require_relative "../repost"

module EpiclackBot
  module Listeners
    class FrameReactionAddListener

      def initialize bot, config
        @bot = bot
        @config = config
      end

      def register
        @bot.reaction_add do |event|
          channel = event.channel
          emoji = event.emoji
          message = event.message
          server = event.server
          user = event.user

          if @config.is_admin_on_server? server, user
            username = message.author.username
            attachments = message.attachments

            $logger.info "Repost trigger on #{server.name} by #{user.username}"

            if emoji.name == $TRIGGER_REPOST_EMOJI
              handle_repost server, attachments, username
            end
          end
        end
      end
    end
  end
end
