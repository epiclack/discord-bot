module EpiclackBot
  module Listeners
    class N1tsuAutoreactListener

      def initialize bot, config
        @bot = bot
        @config = config
      end

      def register
        # Message from n1tsu
        @bot.message(from: 213060213341814784) do |event|
          server = event.server

          if @config.get_server server, "n1tsu_autoreact"
            event.message.react("🤠")
          end
        end

        # Register command toggle autoreact
        @bot.command :n1tsu_autoreact, description: "Toggle n1tsu auto react" do |event|
          server = event.server
          user = event.user

          if @config.is_admin_on_server? server, user
            autoreact = @config.get_server server, "n1tsu_autoreact"

            @config.set_server server, "n1tsu_autoreact", !autoreact

            event << "**N1tsu autoreact** change to: #{!autoreact}"
          end
        end
      end
    end
  end
end
